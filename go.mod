module chainmaker.org/chainmaker/vm-wxvm/v2

go 1.15

require (
	chainmaker.org/chainmaker/common/v2 v2.2.0
	chainmaker.org/chainmaker/pb-go/v2 v2.2.0
	chainmaker.org/chainmaker/protocol/v2 v2.2.1
	chainmaker.org/chainmaker/utils/v2 v2.2.1
	github.com/go-sql-driver/mysql v1.6.0 // indirect
	github.com/google/uuid v1.3.0 // indirect
	github.com/pingcap/errors v0.11.5-0.20201126102027-b0a155152ca3 // indirect
	github.com/pingcap/log v0.0.0-20201112100606-8f1e84a3abc8 // indirect
	golang.org/x/sync v0.0.0-20210220032951-036812b2e83c
)
