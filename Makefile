VERSION220=v2.2.0
VERSION221=v2.2.1
gomod:
	go get chainmaker.org/chainmaker/common/v2@$(VERSION220)
	go get chainmaker.org/chainmaker/protocol/v2@$(VERSION221)
	go get chainmaker.org/chainmaker/pb-go/v2@$(VERSION220)
	go get chainmaker.org/chainmaker/utils/v2@$(VERSION221)
	go mod tidy
	cat go.mod |grep chainmaker